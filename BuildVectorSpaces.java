import java.io.IOException;

import pitt.search.semanticvectors.BuildIndex;
//import pitt.search.semanticvectors.BuildPositionalIndex;
import pitt.search.semanticvectors.BuildPositionalIndex;



public class BuildVectorSpaces {

	

	public BuildVectorSpaces() {
		// TODO Auto-generated constructor stub
		
		
	}
	
	/****** TRRI *******/
	
	//Dr. Cohen recommendation
	public void buildTRRI(String luceneIndexPath,String documentModelName, String termModelName, int cycles, 
			String termWeight, String elementalMethod, String stopWordLocation  ) {
		
		try {
			BuildIndex.main(new String [] {
					"-luceneindexpath", luceneIndexPath, "-trainingcycles", 
					Integer.toString(cycles), "-initialtermvectors", "random",
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName,
					"-termweight", termWeight, "-elementalmethod", elementalMethod, "-stoplistfile", stopWordLocation
			});
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void buildTRRI(String luceneIndexPath,String documentModelName, String termModelName, int cycles) {
		
	
		try {
			BuildIndex.main(new String [] {"-luceneindexpath", luceneIndexPath, "-trainingcycles", 
					Integer.toString(cycles), "-initialtermvectors", "random",
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void buildTRRI(String luceneIndexPath,String documentModelName, String termModelName) {

		
		try {
			
			BuildIndex.main(new String [] {"-luceneindexpath", luceneIndexPath, 
					"-trainingcycles", "2", "-initialtermvectors", "random",
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName
					});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	/*** DRRI ****/
	
	//DR. Cohen's recommendation
	public void buildDRRI(String luceneIndexPath, String documentModelName, String termModelName, int cycles,
			String termWeight, String elementalMethod, String stopListLocation) {
		
		try {
			
			BuildIndex.main(new String [] {"-luceneindexpath", luceneIndexPath, 
					"-trainingcycles", Integer.toString(cycles),
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName,
					"-termweight", termWeight, "-elementalmethod", elementalMethod, "-stoplistfile", stopListLocation
					
			});
			
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void buildDRRI(String luceneIndexPath, String documentModelName, String termModelName, int cycles) {

		
		try {
			BuildIndex.main(new String [] {"-luceneindexpath", luceneIndexPath, 
					"-trainingcycles", Integer.toString(cycles),
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void buildDRRI(String luceneIndexPath, String documentModelName, String termModelName) {
		try {
			BuildIndex.main(
					new String [] {"-trainingcycles", "2",
					"-luceneindexpath", luceneIndexPath,
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName
					});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**** PLAIN RI (Document x Term)***/
	
	//Dr. Cohen's recommendation
	public void buildRI(String luceneIndexPath, String documentModelName, String termModelName,
			String elementalMethod, String stopListLocation) {
		
		try {
			
			BuildIndex.main(new String[] {"-luceneindexpath", luceneIndexPath, 
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName,
					"-elementalmethod", elementalMethod, "-stoplistfile", stopListLocation
					
			});
			
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void buildRI(String luceneIndexPath, String documentModelName, String termModelName) {
		try {
			BuildIndex.main(new String[] {"-luceneindexpath", luceneIndexPath, 
					"-termvectorsfile", termModelName, "-docvectorsfile", documentModelName});
		} catch (IllegalArgumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * 
	 *  POSITIONAL INDEX METHODS...... 
	 * 
	 * */
	
	
	/***** SLIDING WINDOW RI ******/
	
	//Dr. Cohen's recommendation
	public void buildSlidingWindow(String luceneIndexPath, String termVectorModelName, int radius,
			String elementalMethod, String stopListLocation, String docIndexingStrategy) {
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-termtermvectorsfile", termVectorModelName, "-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec",
				"-elementalmethod", elementalMethod, "-stoplistfile", stopListLocation, "-docindexing", docIndexingStrategy
				};
		
		BuildPositionalIndex.main(command);
		
		
	}
	
	public void buildSlidingWindow(String luceneIndexPath, String termVectorModelName, int radius) {


		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-termtermvectorsfile", termVectorModelName, "-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"};

		BuildPositionalIndex.main(command);

	}
	
	public void buildSlidingWindow(String luceneIndexPath, String termVectorModelName) {

		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius","10",
				"-termtermvectorsfile", termVectorModelName, "-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"};

		BuildPositionalIndex.main(command);

	}
	
	/****  HAL   *****/
	
	//Dr. Cohen's recommendation
	public void buildDirectionalWindow(String luceneIndexPath, String termVectorModelName, int radius,
			String elementalMethod, String stopListLocation, String documentIndexStrategy) {
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-positionalmethod","directional",
				"-directionalvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec",
				"-elementalmethod", elementalMethod, "-stoplistfile", stopListLocation, "-docindexing", documentIndexStrategy
				};
		
		BuildPositionalIndex.main(command);
		
		
	}

	public void buildDirectionalHALWindow(String luceneIndexPath, String termVectorModelName, int radius) {


		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-positionalmethod","directional",
				"-directionalvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"};
				
		

		BuildPositionalIndex.main(command);
	}
	
	public void buildDirectionalHALWindow(String luceneIndexPath, String termVectorModelName) {


		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius","10",
				"-positionalmethod","directional",
				"-directionalvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	
	
	/**** SKIP GRAM  ****/
	
	//Dr. Cohen's recommendation
	public void buildSkipgramEmbedding(String luceneIndexPath, String termVectorModelName, int radius, int seedlength, int training_cycles,
			String elementalMethod, String stopListLocation, String documentIndexStrategy, double samplingThreshold, int dimension, boolean subSampleWindow ) {
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius), 
				"-encodingmethod","embeddings", "-seedlength", Integer.toString(seedlength),
				"-embeddingvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec",
				"-elementalmethod", elementalMethod, "-stoplistfile", stopListLocation, "-docindexing", documentIndexStrategy,
				"-trainingcycles", Integer.toString(training_cycles), 
				"-samplingthreshold", Double.toString(samplingThreshold),
				"-dimension", Integer.toString(dimension),
				"-subsampleinwindow", Boolean.toString(subSampleWindow)
				
		};
		
		BuildPositionalIndex.main(command);
		
		
	}
	
	public void buildSkipgramEmbedding(String luceneIndexPath, String termVectorModelName, int radius, int seedlength) {
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius), 
				"-encodingmethod","embeddings", "-seedlength", Integer.toString(seedlength),
				"-embeddingvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	
	
	public void buildSkipgramEmbedding(String luceneIndexPath, String termVectorModelName, int radius) {
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-positionalmethod","embeddings",
				"-embeddingvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	

	public void buildSkipgramEmbedding(String luceneIndexPath, String termVectorModelName) {
		
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius","10",
				"-positionalmethod","embeddings",
				"-embeddingvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	
	public void buildEncodingEmbedding(String luceneIndexPath, String termVectorModelName, int radius,
			int training_cycles) {
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-encodingmethod","embeddings",
				"-trainingcycles", Integer.toString(training_cycles),
				"-embeddingvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	
	public void buildEncodingEmbedding(String luceneIndexPath, String termVectorModelName, int radius,
			int training_cycles, int seedlength) {
		
		String [] command = {"-luceneindexpath", luceneIndexPath,
				"-windowradius",Integer.toString(radius),
				"-encodingmethod","embeddings", "-seedlength", Integer.toString(seedlength),
				"-trainingcycles", Integer.toString(training_cycles),
				"-embeddingvectorfile", termVectorModelName,
				"-docvectorsfile", termVectorModelName +"_docvec", "-elementalvectorfile",
				termVectorModelName +"_elemvec"
		};

		BuildPositionalIndex.main(command);
	}
	
	public static void main(String[] args) throws IllegalArgumentException, IOException {
		// TODO Auto-generated method stub

		
		String luceneLocation = "/path/to/lucene/index/";
		String stopWordLocation = "/path/to/stop_word_list";
		
		BuildVectorSpaces v = new BuildVectorSpaces();
		
		/**  Dr. Cohen's recommended model mods  **/
		v.buildTRRI(luceneLocation, "hpv_doc_trri", "TRRItermvectors", 2, "IDF", "contenthash", stopWordLocation);
		v.buildRI(luceneLocation, "hpv_doc_ri", "RItermvectors", "contenthash", stopWordLocation);
		v.buildDRRI(luceneLocation, "hpv_doc_drri", "DRRItermvectors", 2, "IDF", "contenthash", stopWordLocation);
		v.buildSlidingWindow(luceneLocation, "slidingRIvectors", 10, "contenthash", stopWordLocation, "none");
		v.buildDirectionalWindow(luceneLocation, "directionalRIvectors", 10, "contenthash", stopWordLocation, "none");
		v.buildSkipgramEmbedding(luceneLocation, "skipgramVectors", 10, 200, 9, "contenthash", stopWordLocation,
				"none", .001, 200, true);
		
		
		//v.buildRI(luceneLocation, "hpv_doc_ri_del", "hpv_term_ri_del");
		
		/*v.buildDRRI(luceneLocation, "hpv_doc_drri_tc5", "hpv_doc_drri_tc5", 5);
		v.buildDRRI(luceneLocation, "hpv_doc_drri_tc10", "hpv_doc_drri_tc10", 10);
		v.buildDRRI(luceneLocation, "hpv_doc_drri_tc15", "hpv_doc_drri_tc15", 15);
		v.buildDRRI(luceneLocation, "hpv_doc_drri_tc20", "hpv_doc_drri_tc20", 20);
		
		v.buildTRRI(luceneLocation, "hpv_doc_trri_tc5", "hpv_doc_trri_tc5", 5);
		v.buildTRRI(luceneLocation, "hpv_doc_trri_tc10", "hpv_doc_trri_tc10", 10);
		v.buildTRRI(luceneLocation, "hpv_doc_trri_tc15", "hpv_doc_trri_tc15", 15);
		v.buildTRRI(luceneLocation, "hpv_doc_trri_tc20", "hpv_doc_trri_tc20", 20);*/
		
		/*
		 * v.buildEncodingEmbedding(luceneLocation, "hpv_embedding_10_tc5", 10, 5);
		 * v.buildEncodingEmbedding(luceneLocation, "hpv_embedding_10_tc10", 10, 10);
		 * v.buildEncodingEmbedding(luceneLocation, "hpv_embedding_10_tc15", 10, 15);
		 * v.buildEncodingEmbedding(luceneLocation, "hpv_embedding_10_tc20", 10, 20);
		 */
		
		/*
		
		//Build TxD RI
		v.buildRI(luceneLocation, "hpv_doc_ri", "hpv_term_ri");
		
		//Build TRRI
		v.buildTRRI(luceneLocation, "hpv_doc_trri", "hpv_term_trri");
		
		//Build DRRI
		v.buildDRRI(luceneLocation, "hpv_doc_drri", "hpv_term_drri");
		
		//Build HAL 
		v.buildDirectionalHALWindow(luceneLocation, "hpv_hal_10", 10);
		
		//Build Sliding Window
		v.buildSlidingWindow(luceneLocation, "hpv_dir_10", 10);
		
		//Build Skipgram
		v.buildSkipgramEmbedding(luceneLocation, "hpv_skipgram_10", 10);*/
		
		//v.buildSkipgramEmbedding(luceneLocation, "hpv_skipgram_10_redux", 10, 200); //2019 redo
		
		/*
		  v.buildEncodingEmbedding(luceneLocation, "hpv_embedding_10_tc5_redux", 10, 5, 200);
		  v.buildEncodingEmbedding(luceneLocation, "hpv_embedding_10_tc10_redux", 10, 10, 200);
		  v.buildEncodingEmbedding(luceneLocation, "hpv_embedding_10_tc15_redux", 10, 15, 200);
		  v.buildEncodingEmbedding(luceneLocation, "hpv_embedding_10_tc20_redux", 10, 20, 200);
		*/ 
		
	}

}

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.CharSink;
import com.google.common.io.FileWriteMode;
import com.google.common.io.Files;

import pitt.search.semanticvectors.FlagConfig;
import pitt.search.semanticvectors.SearchResult;
import pitt.search.semanticvectors.VectorSearcher;
import pitt.search.semanticvectors.VectorStoreRAM;
import pitt.search.semanticvectors.vectors.ZeroVectorException;

public class QueryVectors {

	FlagConfig defaultFlagConfig = FlagConfig.getFlagConfig(null);

	private VectorStoreRAM vectorStore = null;

	public QueryVectors() {
		// TODO Auto-generated constructor stub
	}

	public void loadVector(String vectorStoreName) {
		// System.out.println(vectorStoreName);
		try {

			vectorStore = VectorStoreRAM.readFromFile(defaultFlagConfig, vectorStoreName);

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public LinkedList<SearchResult> getAboveThreshold(String concept, float threshold) {


		LinkedList<SearchResult> results = null;
		try {
			VectorSearcher searcher = new VectorSearcher.VectorSearcherCosine(vectorStore, vectorStore, null,
					defaultFlagConfig, new String[] { concept });

			
			results = searcher.getAllAboveThreshold((float) 0.496321429);

		} catch (ZeroVectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		return results;
	}


	public LinkedList<SearchResult> getNearestNeighbor(String concept, int top) {


		LinkedList<SearchResult> results = null;
		try {
			VectorSearcher searcher = new VectorSearcher.VectorSearcherCosine(vectorStore, vectorStore, null,
					defaultFlagConfig, new String[] { concept });

			results = searcher.getNearestNeighbors(top);
			//results = searcher.getAllAboveThreshold((float) 0.496321429);

		} catch (ZeroVectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		return results;
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		


		//names of vector stores
		  String[] vectorStoreName = new String[] { "hpv_term_drri2", "hpv_term_ri",
		  "hpv_dir_10", "hpv_hal_10", "hpv_term_trri2", "hpv_skipgram_10_redux" };
		 

		



		String fileName = "terms3.txt"; 
		String[] concepts = loadConceptsFromFile(fileName);


		File outfile = new File("output_query_vectors.txt");
		CharSink sink = Files.asCharSink(outfile, Charsets.UTF_8, FileWriteMode.APPEND);


		for (String vectorName : vectorStoreName) {
			QueryVectors qv = new QueryVectors();

			qv.loadVector(vectorName);

			sink.write("*******\n");
			sink.write(vectorName + "\n"); 
			sink.write("*******\n");
			for (String concept : concepts) {

				sink.write(concept + "\n");
				qv.getNearestNeighbor(concept, 11).forEach(c -> {

					try {
						String s = c.toSimpleString();
						//s = s.replaceAll(",", " ");
						sink.write("\"" +s + "\"\n");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
			}

		}

		System.out.println("Done");

	}

	static public String[] loadConceptsFromFile(String fileName) {
		File file = new File(fileName);
		String[] concepts = null;
		try {
			List<String> result = Files.readLines(file, Charsets.UTF_8);
			concepts = result.toArray(new String[0]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// add underscore to multi-term concepts and convert to lowercase
		for (int i = 0; i < concepts.length; i++) {
			String concept = concepts[i];
			concept = concept.toLowerCase();
			concept = (concept.contains(" ") ? concept.replaceAll(" ", "_") : concept);
			concepts[i] = concept;
		}

		return concepts;
	}

}

rm *.bin
rm *.prx.txt

export CLASSPATH=/path/to/build/semanticvectors-5.9.jar

java pitt.search.semanticvectors.BuildIndex -luceneindexpath positional_index -initialtermvectors random  -trainingcycles 2 -termweight IDF -elementalmethod contenthash -stoplistfile smartStopwords.txt

mv termvectors2.bin TRRItermvectors.bin

java pitt.search.semanticvectors.BuildIndex -luceneindexpath positional_index  -elementalmethod contenthash -stoplistfile smartStopwords.txt

mv termvectors.bin RItermvectors.bin

java pitt.search.semanticvectors.BuildIndex -luceneindexpath positional_index -trainingcycles 2 -termweight IDF -elementalmethod contenthash -stoplistfile smartStopwords.txt

mv termvectors2.bin DRRItermvectors.bin

java pitt.search.semanticvectors.BuildPositionalIndex -luceneindexpath positional_index -windowradius 10 -stoplistfile smartStopwords.txt -elementalmethod contenthash -docindexing none 
mv termtermvectors.bin slidingRIvectors.bin

java pitt.search.semanticvectors.BuildPositionalIndex -luceneindexpath positional_index -positionalmethod directional   -windowradius 10 -elementalmethod contenthash -docindexing none -stoplistfile smartStopwords.txt
mv drxntermvectors.bin directionalRIvectors.bin

java pitt.search.semanticvectors.BuildPositionalIndex -luceneindexpath positional_index -windowradius 10 -encodingmethod embeddings -elementalmethod contenthash -trainingcycles 9 -docindexing none -samplingthreshold .001 -dimension 200 -seedlength 200 -subsampleinwindow -stoplistfile smartStopwords.txt

